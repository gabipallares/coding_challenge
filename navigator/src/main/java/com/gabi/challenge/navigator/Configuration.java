package com.gabi.challenge.navigator;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix="navigator-service")
public class Configuration {
	
	private String datarepository_host;
	private String datarepository_port;
	public String getDatarepository_host() {
		return datarepository_host;
	}
	public void setDatarepository_host(String datarepository_host) {
		this.datarepository_host = datarepository_host;
	}
	public String getDatarepository_port() {
		return datarepository_port;
	}
	public void setDatarepository_port(String datarepository_port) {
		this.datarepository_port = datarepository_port;
	}
	
	public Configuration(String datarepository_host, String datarepository_port) {
		super();
		this.datarepository_host = datarepository_host;
		this.datarepository_port = datarepository_port;
	}
	protected Configuration() {

	}

}
