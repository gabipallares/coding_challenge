/**
* CityController.java
*
* @author Gabriel Pallares
*/
package com.gabi.challenge.navigator.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.gabi.challenge.datarepository.entity.City;
import com.gabi.challenge.navigator.Configuration;
import com.gabi.challenge.navigator.dijkstra.Path;

@RestController
public class NavigatorController {

	@Autowired
	private Configuration configuration;
	
	/**
	 * city => GET operation to obtain all the cities
	 **/	
	@GetMapping("/test")
	public List<City> test(){
		Path path = new Path(10001L,10009L, configuration.getDatarepository_host(), configuration.getDatarepository_port());
		return path.calculateShortestInTime();
	}
	
	/**
	 * city => GET operation to obtain all the cities
	 **/	
	@GetMapping("/pathShortInTime/from/{idSource}/to/{idDestiny}")
	public List<City> shortestPathInTime(@PathVariable final Long idSource, @PathVariable final Long idDestiny){
		Path path = new Path(idSource,idDestiny, configuration.getDatarepository_host(), configuration.getDatarepository_port());
		return path.calculateShortestInTime();
	}
	
	
	/**
	 * city => GET operation to obtain all the cities
	 **/	
	@GetMapping("/pathShortInSteps/from/{idSource}/to/{idDestiny}")
	public List<City> shortestPathInConnections(@PathVariable final Long idSource, @PathVariable final Long idDestiny){
		Path path = new Path(idSource,idDestiny, configuration.getDatarepository_host(), configuration.getDatarepository_port());
		return path.calculateShortestInConnections();
	}
	
}
