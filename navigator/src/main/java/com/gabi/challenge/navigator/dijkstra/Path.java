/**
* CityController.java
*
* @author Gabriel Pallares
*/
package com.gabi.challenge.navigator.dijkstra;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.Predicate;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.gabi.challenge.datarepository.entity.City;
import com.gabi.challenge.datarepository.entity.Journey;
import com.gabi.challenge.datarepository.exception.ElementNotFoundException;

public class Path {
		
	/**
	 * dataprovider host
	 **/
	private final String host;
	/**
	 * dataprovider port
	 **/
	private final String port;
	/**
	 * Path source city
	 **/
	private City source;
	/**
	 * Path destination city
	 **/
	private City destiny;

	public City getSource() {
		return source;
	}

	public void setSource(City source) {
		this.source = source;
	}

	public City getDestiny() {
		return destiny;
	}

	public void setDestiny(City destiny) {
		this.destiny = destiny;
	}	
	
	/** 
	 *   Call the data provider and recover 
	 *  @param cityId city identificator
	 ***/
	private City getCityDetails(final long cityId) {
		ResponseEntity<City> responseEntity;
		Map<String, String> uriVariables = new HashMap<>();
		try {
			responseEntity= new RestTemplate().getForEntity(
					String.format("http://%s:%s/city/%d", host, port, cityId),
					City.class, 
					uriVariables);
		}
		catch (Exception e) {
			System.out.println(e.toString());
			throw new ElementNotFoundException(String.format("City with the id %d not found",cityId));
		}

		if (responseEntity.getBody()==null) {
			throw new ElementNotFoundException(String.format("City with the id %d not found",cityId));
		}
		return responseEntity.getBody();
	}			

	private List<Journey> getConnections(final long cityId) {
		Map<String, String> uriVariables = new HashMap<>();
		ResponseEntity<List<Journey>> responseEntity = new RestTemplate().exchange(
				String.format("http://%s:%s/journey/cityConections/%d", host, port, cityId),
				HttpMethod.GET,
				null,
				new ParameterizedTypeReference<List<Journey>>(){}, 
				uriVariables);

		List<Journey> response = responseEntity.getBody();
		return response;
	}

	private List<Long> calculatePath(final boolean time)
	{
		Map<Long, Long> pendingExplode = new TreeMap<Long, Long>();
		Set<CityNode> pendingExplodeSorted = new TreeSet<CityNode>();
		Map<Long,Long> reversePath = new HashMap<Long,Long>();
		Set<Long> procesed = new HashSet<Long>();

		long cityIdToProcess=source.getId();
		long lengthPath = 0;
		List<Journey> newConnections;		

		pendingExplode.put(source.getId(), new Long(0));
		pendingExplodeSorted.add(new CityNode(source.getId(), new Long(0)));
		reversePath.put(source.getId(), -1L);				
		while(cityIdToProcess!=destiny.getId()) {
			newConnections =  getConnections(cityIdToProcess);
			for (int i=0;i<newConnections.size();i++) {						
				Journey newJourney = newConnections.get(i);
				//Calculate the time travel (distance)
				long minutes = (time) ? calculateTimeTravel(newJourney) : 1;
				//Calculate destination City
				long destinationCity = (newJourney.getCityDestiny().getId()!=cityIdToProcess) ? newJourney.getCityDestiny().getId() : newJourney.getCitySource().getId();

				if ((!procesed.contains(destinationCity)) &&
						(!pendingExplode.containsKey(destinationCity) ||
								(minutes + lengthPath < pendingExplode.get(destinationCity)))) {
					pendingExplode.put(destinationCity, minutes + lengthPath);
					final long cityId = destinationCity;
					pendingExplodeSorted = updateSortedSet(pendingExplodeSorted, lengthPath, minutes, cityId);
					//Add the processed city to the exploded nodes
					reversePath.put(cityId, cityIdToProcess);
				}
			}
			//Delete the processed city
			procesed.add(cityIdToProcess);
			pendingExplode.remove(cityIdToProcess);
			final long idToDelete = cityIdToProcess;
			Predicate<CityNode> personPredicate = p-> p.getCityId() == idToDelete;
			pendingExplodeSorted.removeIf(personPredicate);
			if (!pendingExplodeSorted.isEmpty()) {
				cityIdToProcess = pendingExplodeSorted.iterator().next().getCityId();
				lengthPath = pendingExplode.get(cityIdToProcess);
			}
			else {
				throw new ElementNotFoundException("No path found to " + destiny.getName());
			}
		}

		ArrayList<Long> shortestPath = new ArrayList<Long>();
		shortestPath.add(cityIdToProcess);
		while (cityIdToProcess!=source.getId()) {
			cityIdToProcess = reversePath.get(cityIdToProcess);
			shortestPath.add(cityIdToProcess);
		}
		Collections.reverse(shortestPath); 
		return shortestPath;
	}

	/**
	 * @param pendingExplodeSorted
	 * @param lengthPath
	 * @param minutes
	 * @param cityId
	 */
	private Set<CityNode> updateSortedSet(Set<CityNode> pendingExplodeSorted, final long lengthPath, final long minutes,
			final long cityId) {

		Predicate<CityNode> cityPredicate = p-> p.getCityId() == cityId;
		pendingExplodeSorted.removeIf(cityPredicate);
		pendingExplodeSorted.add(new CityNode(cityId, minutes + lengthPath));
		return pendingExplodeSorted;
	}

	private long calculateTimeTravel(final Journey newJourney) {
		LocalTime departTime = newJourney.getDepartureTime();
		LocalTime arrivalTime = newJourney.getArrivalTime();
		long minutes = departTime.until(arrivalTime, ChronoUnit.MINUTES);
		return minutes;
	}

	public Path(final City source,final  City destiny,final String host,final String port) {
		super();
		this.host = host;
		this.port = port;
		this.source = source;
		this.destiny = destiny;	
	}

	public Path(final long sourceId, final long destinyId, final String host, final String port) {
		super();
		this.host = host;
		this.port = port;
		this.source=getCityDetails(sourceId);
		this.destiny= getCityDetails(destinyId);	

	}

	public List<City> calculateShortestInTime(){
		List<City> toReturn = new ArrayList<City>();
		List<Long> cityIds = this.calculatePath(true);
		for (Long cityId : cityIds) {
			toReturn.add(this.getCityDetails(cityId));
		}
		return toReturn;
	}

	public List<City> calculateShortestInConnections(){
		List<City> toReturn = new ArrayList<City>();
		List<Long> cityIds = this.calculatePath(false);
		for (Long cityId : cityIds) {
			toReturn.add(this.getCityDetails(cityId));
		}
		return toReturn;
	}
}
