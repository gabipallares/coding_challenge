package com.gabi.challenge.navigator.dijkstra;

public class CityNode implements Comparable<CityNode>{
	
	private long cityId;
	private long pathLength;
	
	
	/**
	 * @return the cityId
	 */
	public long getCityId() {
		return cityId;
	}


	/**
	 * @param cityId the cityId to set
	 */
	public void setCityId(long cityId) {
		this.cityId = cityId;
	}


	/**
	 * @return the pathLength
	 */
	public long getPathLength() {
		return pathLength;
	}


	/**
	 * @param pathLength the pathLength to set
	 */
	public void setPathLength(long pathLength) {
		this.pathLength = pathLength;
	}


	/**
	 * @param cityId
	 * @param pathLength
	 */
	public CityNode(long cityId, long pathLength) {
		super();
		this.cityId = cityId;
		this.pathLength = pathLength;
	}


	@Override
	public int compareTo(CityNode o) {
		// TODO Auto-generated method stub
		if (pathLength< o.pathLength) {
			return -1;
		}
		return 1;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CityNode [cityId=" + cityId + ", pathLength=" + pathLength + "]";
	}

}
