package com.gabi.challenge.navigator.dijkstra;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.gabi.challenge.datarepository.entity.City;
import com.gabi.challenge.datarepository.entity.Journey;
import com.gabi.challenge.datarepository.entity.Path;

public class DetailedJourney extends Journey{
	
	private Path shortPath;
	
	private Path straightPath;
	
	private City source;
	
	private City destiny;

	private boolean calculated;

	/**
	 * @return the shortPath
	 */
	public Path getShortPath() {
		return shortPath;
	}

	/**
	 * @param shortPath the shortPath to set
	 */
	public void setShortPath(Path shortPath) {
		this.shortPath = shortPath;
	}

	/**
	 * @return the straightPath
	 */
	public Path getStraightPath() {
		return straightPath;
	}

	/**
	 * @param straightPath the straightPath to set
	 */
	public void setStraightPath(Path straightPath) {
		this.straightPath = straightPath;
	}

	/**
	 * @return the source
	 */
	public City getSource() {
		return source;
	}

	/**
	 * @param source the source to set
	 */
	public void setSource(City source) {
		this.source = source;
	}

	/**
	 * @return the destiny
	 */
	public City getDestiny() {
		return destiny;
	}

	/**
	 * @param destiny the destiny to set
	 */
	public void setDestiny(City destiny) {
		this.destiny = destiny;
	}

	/**
	 * @return the calculated
	 */
	public boolean isCalculated() {
		return calculated;
	}

	/**
	 * @param calculated the calculated to set
	 */
	public void setCalculated(boolean calculated) {
		this.calculated = calculated;
	}

	/**
	 * @param source
	 * @param destiny
	 */
	public DetailedJourney(City source, City destiny) {
		super();
		this.source = source;
		this.destiny = destiny;
		this.shortPath = new Path();
		this.straightPath = new Path();
		this.calculated = false;
	}
	
	private List<Journey> getConnections(final long cityId) {
		Map<String, String> uriVariables = new HashMap<>();
		ResponseEntity<List> responseEntity = new RestTemplate().getForEntity(
				String.format("http://localhost:8080/journey/fromcity/%d", cityId), 
				List.class, 
				uriVariables);
		
		List<Journey> response = responseEntity.getBody();
		return response;
	}
	
	public void calculate()
	{
		Map<Long, Float> pendingExplode = new TreeMap<Long, Float>();
		Map<Long,Long> exploded = new HashMap<Long,Long>();
		long cityIdToProcess=source.getId();
		long previousCity = -1;
		float lengthPath = 0;
		List<Journey> toInclude;		
		
		pendingExplode.put(this.source.getId(), Float.valueOf(0));
		while(cityIdToProcess!=this.destiny.getId()) {
			toInclude =  getConnections(cityIdToProcess);
			for (int i=0;i<toInclude.size();i++) {
				float minutes = toInclude.get(i).getDepartureTime().until(toInclude.get(i).getArrivalTime(), ChronoUnit.MINUTES);
				if (!pendingExplode.containsKey(toInclude.get(i).getCityDestiny().getId()) ||
						(minutes + lengthPath < pendingExplode.get(toInclude.get(i).getCityDestiny().getId()))) {
					pendingExplode.put(toInclude.get(i).getCityDestiny().getId(), minutes + lengthPath);
				}
			}
			//Delete the processed city
			pendingExplode.remove(cityIdToProcess);
			//Add the processed city to the exploded nodes
			exploded.put(cityIdToProcess, previousCity);
			previousCity = cityIdToProcess;
			//get the first object (the minimun lenght)
			cityIdToProcess = pendingExplode.keySet().iterator().next();
			lengthPath = pendingExplode.get(cityIdToProcess);
		}
		
		ArrayList<Long> shortestPath = new ArrayList<Long>();
		shortestPath.add(cityIdToProcess);
		shortestPath.add(previousCity);
		while (previousCity!=source.getId()) {
			previousCity = exploded.get(previousCity);
			shortestPath.add(previousCity);
		}
		Collections.reverse(shortestPath); 
		System.out.println("Camino calculado " + shortestPath);
		
	}


	
	
	
}
