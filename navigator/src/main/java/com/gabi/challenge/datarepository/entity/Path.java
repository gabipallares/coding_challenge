package com.gabi.challenge.datarepository.entity;

import java.util.ArrayList;

public class Path {
	private ArrayList<City> path;
	private long length;

	
	/**
	 * @return the path
	 */
	public ArrayList<City> getPath() {
		return path;
	}


	/**
	 * @param path the path to set
	 */
	public void setPath(final ArrayList<City> path) {
		this.path = path;
	}


	/**
	 * @return the length
	 */
	public long getLength() {
		return length;
	}


	/**
	 * @param length the length to set
	 */
	public void setLength(final long length) {
		this.length = length;
	}


	public void addCity (final City city, final long leght) {
		this.path.add(city);
	}


	/**
	 * 
	 */
	public Path() {
		super();
		length = 0;
		path = new ArrayList<City>();
	}

	
	
}
