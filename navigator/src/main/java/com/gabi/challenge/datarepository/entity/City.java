/**
* City.java
*
* @author Gabriel Pallares
*/
package com.gabi.challenge.datarepository.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;



@Entity
@NamedQueries({
	@NamedQuery(name="get_all_cities", query="SELECT c FROM City c")          
})
public class City {
	
	/**
	 * id, primary key
	 **/
	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * name of the city
	 **/
	private String name;

	/**
	 * province where the city belongs to
	 **/
	private String province;

	public Long getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getProvince() {
		return province;
	}
	
	
	/** Default constructor **/
	public City() {
		super();
	}

	/** 
	 *   Constructor with parameters
	 *  @param name contains the city name
	 *  @param province contains the name of the region which the city belongs to
	 ***/
	public City(final String name, final String province) {
		super();
		this.name = name;
		this.province = province;
	}

	public void setProvince(final String province) {
		this.province = province;
	}


	@Override
	public String toString() {
		return "City [id=" + id + ", name=" + name + ", province=" + province + "]";
	}
	
	

}
