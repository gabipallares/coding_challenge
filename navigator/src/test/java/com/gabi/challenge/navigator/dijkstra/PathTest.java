package com.gabi.challenge.navigator.dijkstra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.gabi.challenge.datarepository.entity.City;
import com.gabi.challenge.datarepository.exception.ElementNotFoundException;
import com.gabi.challenge.navigator.Configuration;

import junit.framework.Assert;

public class PathTest {

    @Autowired
    private Environment environment;
    private final Long sourceId = 10001L;
    private final Long destinyId = 10009L;
    private final List<Long> pathShortTestInTime = new ArrayList<Long>(Arrays.asList(10001L, 10002L, 10008L, 10009L));
    private final List<Long> pathShortTestInConn = new ArrayList<Long>(Arrays.asList(10001L, 10004L, 10009L));
    
	private final String host = "localhost";
	private final String port = "8090";
	
	@Test(expected=ElementNotFoundException.class)
	public void sourceCityUndefined() {
		new Path(-1, 10001, 
				host, 
				port);		
	}
	
	@Test(expected=ElementNotFoundException.class)
	public void destinyCityUndefined() {
		new Path(10001, -1, 
				host, 
				port);					
	}
	
	@Test()
	public void exists() {
		Path path = new Path(10001, 10009, 
				host, 
				port);
		assertNotNull(path);
	}
	
	
	@Test()
	public void pathShortInTime() {
		Path path = new Path(10001, 10009, 
				host, 
				port);
		
		List<City> cities = path.calculateShortestInTime();
		List<Long> returned = new ArrayList<Long>();
		for( City c : cities){
			returned.add(c.getId());
		}
		assertArrayEquals(pathShortTestInTime.toArray(),returned.toArray() );
	}
	
	@Test()
	public void pathShortInSteps() {
		Path path = new Path(10001, 10009, 
				host, 
				port);
		
		List<City> cities = path.calculateShortestInConnections();
		List<Long> returned = new ArrayList<Long>();
		for( City c : cities){
			returned.add(c.getId());
		}
		assertArrayEquals(pathShortTestInConn.toArray(),returned.toArray() );
	}

}
