/**
* DataRepositoryApplicationTests.java
*
* @author Gabriel Pallares
*/

package com.gabi.challenge.datarepository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DataRepositoryApplicationTests {

	/** 
	 *   Test to see if everything is up & running
	 ***/
	@Test
	public void contextLoads() {
		//Test to see if everything is up and running
		assert(true);
	}

	public DataRepositoryApplicationTests() {
		super();
	}	

}
