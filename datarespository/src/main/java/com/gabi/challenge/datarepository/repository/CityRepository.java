/**
* CityRepository.java
*
* @author Gabriel Pallares
*/

package com.gabi.challenge.datarepository.repository;

import java.util.List;

import javax.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.gabi.challenge.datarepository.entity.City;
import com.gabi.challenge.datarepository.exception.ElementNotFoundException;

@Repository
public class CityRepository {

	/** 
	 *   Entity Manager to store all the cities
	 ***/

	@Autowired
	private EntityManager em;
	
	
	/** 
	 *   Retrieve all the Cities
	 ***/
	public List<City> findAll() {
		return em.createNamedQuery("get_all_cities", City.class).getResultList();
	}
	
	/** 
	 *   Recover the City with id = id
	 ***/
	public City findById(final Long id) {
		final City toReturn =  em.find(City.class, id);
		if (toReturn == null)
		{
			throw new ElementNotFoundException(String.format("City with id %d not found", id));
		}
		return toReturn;
	}
	
	/** 
	 *   Persist the City update or save
	 ***/
    public City save(final City city){
        if (city.getId()==null){
            //New Item => Insert
            em.persist(city);
        }
        else{
            //Edit Item => Update
            em.merge(city);
        }
        return city;
    }

	/** 
	 *   Delete the City with the given id
	 ***/
    public void deleteById(final Long id){
    	final City city = findById(id);
        if (city != null){
            em.remove(city);
        }

    }	
	

}
