/**
* Journey.java
*
* @author Gabriel Pallares
*/
package com.gabi.challenge.datarepository.entity;

import java.time.LocalTime;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
	@NamedQuery(name="get_all_journeys", query="SELECT j FROM Journey j")          
})
public class Journey {
	
	/**
	 *  id
	 **/
	@Id
	@GeneratedValue
	private Long id;	
	
	/**
	 * city source entity
	 **/
	@ManyToOne(fetch=FetchType.EAGER)
	private City citySource;

	/**
	 * city destiny entity
	 **/
	@ManyToOne(fetch=FetchType.EAGER)
	private City cityDestiny;
	
	/**
	 * Departure time from source city
	 **/
	private LocalTime departureTime;	
		
	/**
	 * Arrival time to the destination city
	 **/
	private LocalTime arrivalTime;	

	public Long getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public LocalTime getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(final LocalTime departureTime) {
		this.departureTime = departureTime;
	}

	public LocalTime getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(final LocalTime arrivalTime) {
		this.arrivalTime = arrivalTime;
	}


	/** Default constructor **/
	public Journey() {
		super();
	}

	/** Constructor with parameters
	 *  @param citySource the source city id
	 *  @param cityDestiny the destiny city id  
	 *  @param departureTime 
	 *  @param arrivalDate   
	 ***/
	public Journey(final City citySource, final City cityDestiny, final LocalTime departureTime, final LocalTime arrivalTime) {
		super();
		this.citySource = citySource;
		this.cityDestiny = cityDestiny;
		this.departureTime = departureTime;
		this.arrivalTime = arrivalTime;
	}

	@Override
	public String toString() {
		return "Journey [id=" + id + citySource.getId() +  ", cityDestinyId=" + cityDestiny.getId() + ", departureTime=" + departureTime
				+ ", arrivalDate=" + arrivalTime + "]";
	}

	public City getCitySource() {
		return citySource;
	}

	public void setCitySource(final City citySource) {
		this.citySource = citySource;
	}

	public City getCityDestiny() {
		return cityDestiny;
	}

	public void setCityDestiny(final City cityDestiny) {
		this.cityDestiny = cityDestiny;
	}


	
}
