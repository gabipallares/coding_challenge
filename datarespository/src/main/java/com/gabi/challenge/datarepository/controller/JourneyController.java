/**
* CityController.java
*
* @author Gabriel Pallares
*/
package com.gabi.challenge.datarepository.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.gabi.challenge.datarepository.entity.Journey;
import com.gabi.challenge.datarepository.repository.JourneyRepository;

@RestController
public class JourneyController {
	/**
	 * service the journeys repository
	 **/
	@Autowired
	private JourneyRepository service; 	
	
	/**
	 * journey => GET operation to obtain all the journeys
	 **/
	@GetMapping("/journey")
	public List<Journey> getAllJourneys(){
		return service.findAll();
				
	}

	/**
	 * journey/{id} => GET operation to obtain a journey with its id
	 **/
	@GetMapping("/journey/{id}")
	public Journey getJourneyById(@PathVariable final Long id){
		return service.findById(id);
				
	}	
	
	/**
	 * journey/from/{id} => GET operation to obtain all journeys from a city
	 **/
	@GetMapping("/journey/fromcity/{id}")
	public List<Journey> getJourneysFromId(@PathVariable final Long id){
		return service.journeysFromCity(id);
				
	}	
	
	/**
	 * journey/from/{id} => GET operation to obtain all journeys from a city
	 **/
	@GetMapping("/journey/cityConections/{id}")
	public List<Journey> getConnectionsFromId(@PathVariable final Long id){
		return service.cityConnections(id);
				
	}	
}
