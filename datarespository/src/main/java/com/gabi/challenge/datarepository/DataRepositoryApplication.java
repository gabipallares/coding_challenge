/**
* DataRepositoryApplication.java
*
* @author Gabriel Pallares
*/

package com.gabi.challenge.datarepository;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class DataRepositoryApplication {

	/** 
	 *   Application starting point
	 *  @param args
	 ***/
	public static void main(final String[] args) {
		SpringApplication.run(DataRepositoryApplication.class, args);
	}
	
}
