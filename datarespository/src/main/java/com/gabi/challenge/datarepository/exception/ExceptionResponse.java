/**
* ExceptionResponse.java
*
* @author Gabriel Pallares
*/

package com.gabi.challenge.datarepository.exception;

import java.util.Date;

public class ExceptionResponse {
	/**
	 * Exception time
	 **/
	private final Date timestamp;
	/**
	 * Exception message
	 **/
	private final String message;
	/**
	 * Exception message details
	 **/
	private final String details;

	
	/** 
	 *   Constructor with parameters
	 *  @param timestamp exception time
	 *  @param message exception message
	 *  @param details exception details
	 ***/
	public ExceptionResponse(Date timestamp, String message, String details) {
		super();
		this.timestamp = timestamp;
		this.message = message;
		this.details = details;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public String getMessage() {
		return message;
	}

	public String getDetails() {
		return details;
	}
}
