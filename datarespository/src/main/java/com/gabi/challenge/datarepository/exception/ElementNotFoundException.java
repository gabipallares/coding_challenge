/**
* ElementNotFoundException.java
*
* @author Gabriel Pallares
*/

package com.gabi.challenge.datarepository.exception;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ElementNotFoundException extends RuntimeException {

	/** 
	 *   Constructor
	 *   @param message.the message detail
	 ***/
	public ElementNotFoundException(final String message) {
		super(message);
	}

	
}
