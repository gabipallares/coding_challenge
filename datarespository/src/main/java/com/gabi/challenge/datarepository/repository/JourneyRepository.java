/**
* CityRepository.java
*
* @author Gabriel Pallares
*/

package com.gabi.challenge.datarepository.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.gabi.challenge.datarepository.entity.Journey;
import com.gabi.challenge.datarepository.exception.ElementNotFoundException;

@Repository
public class JourneyRepository {
	
	/** 
	 *   Entity Manager to store all the jouneys
	 ***/
	@Autowired
	private EntityManager em;
	
	
	/** 
	 *   Recover the City with id = id
	 ***/
	public List<Journey> findAll() {
		return em.createNamedQuery("get_all_journeys", Journey.class).getResultList();
	}
	
	/** 
	 *   Recover the City with id = id
	 ***/
	public Journey findById(final Long id) {
		final Journey toReturn = em.find(Journey.class, id);
		if (toReturn == null)
		{
			throw new ElementNotFoundException(String.format("Journey with id %d not found", id));
		}
		return toReturn;		
	}
	
	/** 
	 *   Persist the Journey update or save
	 ***/	
    public Journey save(final Journey journey){
        if (journey.getId()==null){
            //New Item => Insert
            em.persist(journey);
        }
        else{
            //Edit Item => Update
            em.merge(journey);
        }
        return journey;
    }

	/** 
	 *   Delete the Journey with the given id
	 ***/
    public void deleteById(final Long id){
    	final Journey journey = findById(id);
        if (journey != null){
            em.remove(journey);
        }

    }	
    
	/** 
	 *   List of journeys with one city as source
	 *   @param id the city id
	 ***/
    public  List<Journey> journeysFromCity(final Long id){
    	Query query = em.createNativeQuery("Select * from Journey where CITY_SOURCE_ID = :id", Journey.class);
        query.setParameter("id", id);
        return query.getResultList();
    }

	public List<Journey> cityConnections(Long id) {
		// TODO Auto-generated method stub
    	Query query = em.createNativeQuery("Select * from Journey where CITY_SOURCE_ID = :id OR CITY_DESTINY_ID = :id", Journey.class);
        query.setParameter("id", id);
        return query.getResultList();
	}    

}
