/**
* CityController.java
*
* @author Gabriel Pallares
*/
package com.gabi.challenge.datarepository.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.gabi.challenge.datarepository.entity.City;
import com.gabi.challenge.datarepository.exception.ElementNotFoundException;
import com.gabi.challenge.datarepository.repository.CityRepository;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@ApiModel(value="All the opreation related with the city entity")
public class CityController {
	/**
	 * service the cities repository
	 **/
	@Autowired
	private CityRepository service; 	
	
	/**
	 * city => GET operation to obtain all the cities
	 **/	
	@GetMapping("/city")
	@ApiOperation(value = "Returns all the cities in the database")
	@ApiResponses( value = { 
				      @ApiResponse(code = 400, message = "Details of error accessing the API", response = ElementNotFoundException.class),
				      @ApiResponse(code = 200, message = "Ended properlly", response = City.class, responseContainer = "List")
				 })
	public List<City> getAllCities(){
		return service.findAll();
				
	}
	
	/**
	 * city/{id} => GET operation to obtain a city with its id
	 **/
	@GetMapping("/city/{id}")
	@ApiOperation(value = "Returns the city info with the id received by parameter")
	@ApiResponses( value = { 
				      @ApiResponse(code = 400, message = "Details of error accessing the API", response = ElementNotFoundException.class),
				      @ApiResponse(code = 200, message = "Ended properlly", response = City.class)
				 })
	public City getCityById(@PathVariable final Long id){
		return service.findById(id);
				
	}	
}
