insert into city(id, name, province) values(10001,'Zaragoza','Zaragoza');
insert into city(id, name, province) values(10002,'Huesca','Huesca');
insert into city(id, name, province) values(10003,'Teruel','Teruel');
insert into city(id, name, province) values(10004,'Lerida','Lerida');
insert into city(id, name, province) values(10005,'Barcelona','Barcelona');
insert into city(id, name, province) values(10006,'Tarragona','Tarragona');
insert into city(id, name, province) values(10007,'Gerona','Gerona');
insert into city(id, name, province) values(10008,'Barbastro','Huesca');
insert into city(id, name, province) values(10009,'Binefar','Huesca');
insert into city(id, name, province) values(10010,'Castellon','Castellon');

insert into journey(id, city_Source_Id, city_Destiny_Id, departure_Time, arrival_Time) values(20001,10001,10002,'17:00:00', '18:00:00');
insert into journey(id, city_Source_Id, city_Destiny_Id, departure_Time, arrival_Time) values(20002,10001,10003,'17:00:00', '18:44:00');
insert into journey(id, city_Source_Id, city_Destiny_Id, departure_Time, arrival_Time) values(20003,10001,10004,'17:00:00', '18:35:00');
insert into journey(id, city_Source_Id, city_Destiny_Id, departure_Time, arrival_Time) values(20004,10002,10004,'17:00:00', '18:15:00');
insert into journey(id, city_Source_Id, city_Destiny_Id, departure_Time, arrival_Time) values(20005,10005,10006,'17:00:00', '18:15:00');
insert into journey(id, city_Source_Id, city_Destiny_Id, departure_Time, arrival_Time) values(20006,10004,10006,'17:00:00', '18:00:00');
insert into journey(id, city_Source_Id, city_Destiny_Id, departure_Time, arrival_Time) values(20007,10005,10007,'17:00:00', '18:14:00');
insert into journey(id, city_Source_Id, city_Destiny_Id, departure_Time, arrival_Time) values(20008,10002,10008,'17:00:00', '17:38:00');
insert into journey(id, city_Source_Id, city_Destiny_Id, departure_Time, arrival_Time) values(20009,10008,10009,'17:00:00', '17:20:00');
insert into journey(id, city_Source_Id, city_Destiny_Id, departure_Time, arrival_Time) values(20010,10009,10004,'17:00:00', '17:35:00');